(() => {
    'use strict'
    const descriptors_needed = 5
    const recognition_video = document.querySelector('#recognition_video')
    let loopRecognition
    let enough_descriptors = false
    let start_matching = false
    let faceMatcher
    let descriptors = []
    let waiting_facialdata_from_login = false
    let progress_text = document.querySelector('#progress p')

    async function loadModels() {
        addProgress('Initializing the facial recognition... Make sure your face is visible on the camera. This process can take up to 10 seconds on a desktop and 20 seconds on a mobile.')
        await faceapi.nets.tinyFaceDetector.loadFromUri('/models')
        await faceapi.nets.faceLandmark68Net.loadFromUri('/models')
        await faceapi.nets.faceRecognitionNet.loadFromUri('/models')
        await faceapi.nets.faceExpressionNet.loadFromUri('/models')
        await faceapi.nets.ageGenderNet.loadFromUri('/models')
        startCamera()
    }

    function startCamera() {
        addProgress('Starting the camera... Please give permissions to start the facial recognition.')
        navigator.mediaDevices.getUserMedia({
            video: true
        })
        .then(mediaStream => {
            addProgress('Initializing the video...')
            recognition_video.srcObject = mediaStream
        })
        .then(() => new Promise(resolve => recognition_video.onloadedmetadata = resolve))
        .then(() => {
            let camera_width = recognition_video.videoWidth
            let camera_height = recognition_video.videoHeight
            document.querySelector('#recognition_video_wrapper').style.minHeight = camera_height + 'px'
            document.querySelector('#recognition_video_wrapper').style.minWidth = camera_width + 'px'
            recognition_video.width = camera_width
            recognition_video.height = camera_height
        })
    }

    function addProgress(text, state) {
        document.querySelector('#progress').style.display = 'block'
        progress_text.classList.remove('failed', 'success')
        if(state === 'success') {
         progress_text.classList.remove('failed')
            progress_text.classList.add('success')
            
        }
        if(state === 'failed') {
            progress_text.classList.remove('success')
            progress_text.classList.add('failed')
        
        }
        progress_text.textContent = text
    }

    function clearCanvas() {
        let canvas = document.querySelector('canvas')
        let context = canvas.getContext('2d')
        context.clearRect(0, 0, canvas.width, canvas.height)
    }

    async function saveFacialData(descriptors_to_save) {
        if(!enough_descriptors) {
            const results = await faceapi
                .detectSingleFace(recognition_video, new faceapi.TinyFaceDetectorOptions())
                .withFaceLandmarks()
                .withFaceDescriptor()
            if (!results) {
                return
            } else {
                descriptors.push(results)
                if(descriptors.length === descriptors_to_save) {
                    addProgress('Saving facial data...')
                    if(!waiting_facialdata_from_login) {
                        enough_descriptors = true
                        let username = document.querySelector('#username_register').value
                        let password = document.querySelector('#password_register').value
                        socket.emit('register', { facialdata: descriptors, username: username, password: password })
                    } else {
                        enough_descriptors = true
                        socket.emit('login', { facialdata: descriptors })
                    }
                }
            }
        }
    }

    socket.on('login', (result) => {
        if(result.reason === 'RECOGNITION_STARTED') {
            waiting_facialdata_from_login = true
            document.querySelector('#recognition_video_wrapper').style.display = 'block'
            loadModels()
        }
        if(result.success === true && result.username) {
            localStorage.setItem('username', result.username)
            document.querySelector('#buttons').style.display = 'none'
            document.querySelector('#login').style.display = 'none'
            document.querySelector('#logout').style.display = 'block'
            document.querySelector('#loggedin').style.display = 'block'
            document.querySelector('#username_txt').textContent = localStorage.getItem('username')
            addProgress('Success! Face verified.', 'success')
        }
        if(result.success === false && result.reason === 'FACES_DONT_MATCH') {
            addProgress('Facial recognition failed. Please try to login again.', 'failed')
            clearCanvas()
            setTimeout(() => {
                clearInterval(loopRecognition)
                clearCanvas()
            }, 600)
            enough_descriptors = false
            descriptors = []
            clearInterval(loopRecognition)
        }
        if(result.success === true && result.reauthed === true) {
            document.querySelector('#loggedin').style.display = 'block'
            document.querySelector('#login').style.display = 'none'
            document.querySelector('#username_txt').textContent = localStorage.getItem('username')
            document.querySelector('#buttons').style.display = 'none'
            
        }
        if(result.success === false && result.reason === 'INVALID_PASSWORD') {
            addProgress('Wrong username or password', 'failed')
        }
    })

    socket.on('register', (result) => {
        if(result.success === false && result.reason === 'USERNAME_ALREADY_EXISTS') {
            addProgress('Username already exists.', 'failed')
            document.querySelector('#username_register').disabled = false
            document.querySelector('#password_register').disabled = false
            document.querySelector('#start_recognition').disabled = false
            clearInterval(loopRecognition)
            setTimeout(() => {
                clearInterval(loopRecognition)
                clearCanvas()
            }, 600)
            clearCanvas()
            enough_descriptors = false
            descriptors = []
        }
        if(result.success === true) {
            let cancel_btn = document.getElementById('cancel_reg')
            cancel_btn.parentNode.removeChild(cancel_btn)
            document.querySelector('#login_btn_registeration').style.display = 'block'
            document.querySelector('#register').style.display = 'none'
            addProgress('Registeration complete! We now have enough reference facial data associated to your username. Try to login.', 'success')
            localStorage.setItem('username', result.username)
        }
    })

    async function drawOnCanvas(canvas, display_size) {
        const detections = await faceapi.detectSingleFace(recognition_video, new faceapi.TinyFaceDetectorOptions())
            .withFaceLandmarks()
            .withFaceExpressions()
            .withAgeAndGender()
        if(detections) {
            const resized_detections = faceapi.resizeResults(detections, display_size)
            clearCanvas()
            faceapi.draw.drawDetections(canvas, resized_detections)
            faceapi.draw.drawFaceLandmarks(canvas, resized_detections)
            faceapi.draw.drawFaceExpressions(canvas, resized_detections)
            
            /* Mean Age Errors: https://github.com/justadudewhohacks/face-api.js?files=1#models-age-and-gender-recognition */
            let MAE = 0
            if(detections.age <= 3.99) {
                MAE = 1.52
            }
            if(detections.age >= 4 && detections.age <= 8.99) {
                MAE = 3.06
            }
            if(detections.age >= 9 && detections.age <= 17.99) {
                MAE = 4.82
            }
            if(detections.age >= 18 && detections.age <= 28.99) {
                MAE = 4.99
            }
            if(detections.age >= 29 && detections.age <= 39.99) {
                MAE = 5.43
            }
            if(detections.age >= 41 && detections.age <= 59.99) {
                MAE = 4.94
            }
            if(detections.age >= 60 && detections.age <= 79.99) {
                MAE = 6.17
            }
            if(detections.age >= 80) {
                MAE = 9.91
            }

            const text = [
                'Gender: ' + detections.gender + ' (probability: ' + Math.round(detections.genderProbability * 100) + '%)',
                'Age: ' + Math.round(detections.age) + ' (± ' + MAE + ' years)'
            ]
            const coords = {
                x: 0,
                y: 0
            }
            const drawOptions = {
                anchorPosition: 'TOP_LEFT',
                backgroundColor: 'rgba(0, 0, 0, 0.59)'
            }
            
            const draw_box = new faceapi.draw.DrawTextField(text, coords, drawOptions)
            draw_box.draw(canvas)
            canvas.style.display = 'block'
        } else {
            canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
        }
    }

    recognition_video.addEventListener('loadeddata', () => {
        document.querySelector('#recognition_video_wrapper').style.display = 'block'
        addProgress('Initializing the facial recognition... Make sure your face is visible on the camera. This process can take up to 10 seconds on a desktop and 20 seconds on a mobile.')
        const canvas = faceapi.createCanvasFromMedia(recognition_video)
        
        if(document.querySelectorAll('#recognition_video_wrapper canvas').length >= 1) {
            let old_canvas = document.querySelectorAll('#recognition_video_wrapper canvas')[0]
            old_canvas.parentNode.removeChild(old_canvas)
        }

        document.querySelector('#recognition_video_wrapper').append(canvas)
        
        const display_size = {
            width: recognition_video.width,
            height: recognition_video.height
        }
        
        faceapi.matchDimensions(canvas, display_size)
        recognition_video.style.display = 'block'
        loopRecognition = setInterval(async () => {
            saveFacialData(descriptors_needed)
            drawOnCanvas(canvas, display_size)
        }, 500)
    })

    const on = (element, event, selector, handler) => {
        element.addEventListener(event, e => {
            if (e.target.closest(selector)) {
                handler(e)
            }
        })
    }

    on(document, 'click', '#start_recognition', e => {
        let register_username = document.querySelector('#username_register').value
        let register_password = document.querySelector('#password_register').value
        let username_regx = new RegExp('^[a-zA-Z0-9]{2,32}$')
        let valid_username = username_regx.test(register_username)
        let errors = false
        
        if(register_password.length <= 1) {
            document.querySelector('.reg-pw .mdc-text-field-helper-line').style.display = 'flex'
            errors = true
        }
        
        if(register_username.length <= 1 || !valid_username) {
            document.querySelector('.reg-un .mdc-text-field-helper-line').style.display = 'flex'
            errors = true
        }
        
        if(!errors) {
            e.target.setAttribute('disabled', 'true')
            document.querySelector('#username_register').setAttribute('disabled', 'true')
            document.querySelector('#password_register').setAttribute('disabled', 'true')
            document.querySelector('#progress').style.display = 'block'
            window.scrollTo(0, (progress_text.offsetTop - 20))
            loadModels()
        }
    })

    on(document, 'keyup', '#username_register, #username_login', e => {
        let val = e.target.value
        let username_regx = new RegExp('^[a-zA-Z0-9]{2,32}$')
        let valid_username = username_regx.test(val)
        let errors = false

        if(val.length <= 1 || val.length >= 32) {
            errors = true
        }

        if(!valid_username) {
            errors = true
        }

        if(errors) {
            e.target.parentNode.classList.add('mdc-text-field--invalid')
            e.target.setAttribute('data-disabled', 'true')
            e.target.classList.add('invalid')
            let label = e.target.nextElementSibling
            let ripple = label.nextElementSibling
            label.classList.add('invalid')
            ripple.classList.add('invalid')
        } else {
            document.querySelector('.reg-un .mdc-text-field-helper-line').style.display = 'none'
            e.target.setAttribute('data-disabled', 'false')
            e.target.parentNode.classList.remove('mdc-text-field--invalid')
            e.target.classList.remove('invalid')
            let label = e.target.nextElementSibling
            let ripple = label.nextElementSibling
            label.classList.remove('invalid')
            ripple.classList.remove('invalid')
        } 
    })

    on(document, 'keyup', '#password_register, #password_login', e => {
        let val = e.target.value
        let errors = false

        if(val.length <= 1) {
            errors = true
        }
        if(errors) {
            e.target.parentNode.classList.add('mdc-text-field--invalid')
            e.target.setAttribute('data-disabled', 'true')
            e.target.classList.add('invalid')
            let label = e.target.nextElementSibling
            let ripple = label.nextElementSibling
            label.classList.add('invalid')
            ripple.classList.add('invalid')
        } else {
            document.querySelector('.reg-pw .mdc-text-field-helper-line').style.display = 'none'
            e.target.setAttribute('data-disabled', 'false')
            e.target.parentNode.classList.remove('mdc-text-field--invalid')
            e.target.classList.remove('invalid')
            let label = e.target.nextElementSibling
            let ripple = label.nextElementSibling
            label.classList.remove('invalid')
            ripple.classList.remove('invalid')
        }  
    })

    on(document, 'submit', '#register, #login', e => {
        e.preventDefault()
    })

    on(document, 'click', '#register_go', e => {
        e.preventDefault()
        e.target.style.display = 'none'
        document.querySelector('#login').style.display = 'none'
        document.querySelector('#buttons').style.display = 'none'
        document.querySelector('#register').style.display = 'block'
    })

    on(document, 'click', '#cancel_reg', e => {
        e.preventDefault()
        
        document.querySelector('#register').style.display = 'none'
        document.querySelector('#register_go').style.display = 'initial'
    })

    on(document, 'click', '#logout', e => {
        socket.emit('logout')
        localStorage.clear()
        window.location.reload()
    })

    on(document, 'click', '#cancel_reg', e => {
        socket.emit('logout')
        window.location.reload()
    })

    on(document, 'click', '#cancel_login', e => {
        socket.emit('logout')
        window.location.reload()
    })

    on(document, 'click', '#login_btn_registeration', e => {
        window.scrollTo(0, 0)
        let username_input = document.querySelector('#username_register')
        let password_input = document.querySelector('#password_register')
        username_input.value = ''
        password_input.value = ''
        username_input.disabled = false
        password_input.disabled = false
        document.querySelector('#start_recognition').disabled = false
        document.querySelector('#register').style.display = 'none'
        clearInterval(loopRecognition)
        setTimeout(() => {
            clearInterval(loopRecognition)
            clearCanvas()
        }, 600)
        recognition_video.style.display = 'none'
        e.target.style.display = 'none'
        document.querySelector('#login').style.display = 'block'
        addProgress('')
        document.querySelector('#progress').style.display = 'none'
        document.querySelector('#recognition_video_wrapper').style.display = 'none'
        clearCanvas()
        enough_descriptors = false
        descriptors = []
    })

    on(document, 'click', '#login_go', e => {
        document.querySelector('#login').style.display = 'block'
        document.querySelector('#buttons').style.display = 'none'
    })

    on(document, 'click', '#login_btn', e => {
        let username = document.querySelector('#username_login').value
        let password = document.querySelector('#password_login').value
        socket.emit('login', { username: username, password: password })
    })

    window.scrollTo(0,0)
})()
